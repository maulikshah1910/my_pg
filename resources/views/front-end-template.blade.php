<!DOCTYPE html >
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>{!! env('APP_NAME') !!}</title>

        <!-- Mobile Specific Metas ================================================== -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="description" content="MyPG App" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=5.0" />

        <!-- Favicon ================================================== -->
        <link rel="icon" type="image/png" href="{!! asset('assets/images/icons/app-favicon.png') !!}" />

        <!-- CSS ================================================== -->
        <!-- Bootstrap -->
        <link rel="stylesheet" href="{!! asset('assets/plugins/bootstrap/bootstrap.min.css') !!}" />
        <!-- FontAwesome -->
        <link rel="stylesheet" href="{!! asset('assets/plugins/fontawesome/css/all.min.css') !!}" />
        <!-- Animation -->
        <link rel="stylesheet" href="{!! asset('assets/plugins/animate-css/animate.css') !!}" />
        <!-- slick Carousel -->
        <link rel="stylesheet" href="{!! asset('assets/plugins/slick/slick.css') !!}" />
        <link rel="stylesheet" href="{!! asset('assets/plugins/slick/slick-theme.css') !!}" />
        <!-- Colorbox -->
        <link rel="stylesheet" href="{!! asset('assets/plugins/colorbox/colorbox.css') !!}" />
        <!-- Template styles-->
        <link rel="stylesheet" href="{!! asset('assets/css/style.css') !!}" />

        @yield('header_content')
    </head>
    <body>
        <div class="body-inner">
            <header id="header" class="header-two">
                <div class="site-navigation">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <nav class="navbar navbar-expand-lg navbar-light p-0">
                                    <div class="logo">
                                        <a class="d-block" href="index-2.html">
                                            <img loading="lazy" src="{!! asset('assets/images/app-logo.png') !!}" alt="{!! env('APP_NAME') !!}" />
                                        </a>
                                    </div><!-- logo end -->
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </header>

            @yield('page_content')

            <footer id="footer" class="footer bg-overlay">
                <div class="footer-main">
                    <div class="container">
                        <div class="row justify-content-between">
                            <div class="col-lg-4 col-md-6 footer-widget footer-about">
                                <h3 class="widget-title">About Us</h3>
                                <img loading="lazy" width="200px" class="footer-logo" src="{!! asset('assets/images/logo-footer.png') !!}" alt="{!! env('APP_NAME') !!}" />
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor inci done idunt ut labore et dolore magna aliqua.
                                </p>
                            </div>

                            <div class="col-lg-4 col-md-6 footer-widget mt-5 mt-md-0">
                            </div>

                            <div class="col-lg-3 col-md-6 mt-5 mt-lg-0 footer-widget">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="copyright">
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-md-6">
                                <div class="copyright-info text-center text-md-left">
                                    <span>
                                        Copyright &copy; {!! \Illuminate\Support\Carbon::today()->format('Y') !!}
                                        {!! env('APP_NAME') !!}
                                        - All rights reserved
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="copyright-info text-center text-md-right">
                                    <span> Powered by KN TechnoLabs </span>
                                </div>
                            </div>
                        </div>

                        <div id="back-to-top" data-spy="affix" data-offset-top="10" class="back-to-top position-fixed">
                            <button class="btn btn-primary" title="Back to Top">
                                <i class="fa fa-angle-double-up"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </footer>
        </div>

    </body>


    <!-- Javascript Files ================================================== -->

    <!-- initialize jQuery Library -->
    <script src="{!! asset('assets/plugins/jQuery/jquery.min.js') !!}"></script>
    <!-- Bootstrap jQuery -->
    <script src="{!! asset('assets/plugins/bootstrap/bootstrap.min.js') !!}" defer></script>
    <!-- Slick Carousel -->
    <script src="{!! asset('assets/plugins/slick/slick.min.js') !!}"></script>
    <script src="{!! asset('assets/plugins/slick/slick-animation.min.js') !!}"></script>
    <!-- Color box -->
    <script src="{!! asset('assets/plugins/colorbox/jquery.colorbox.js') !!}"></script>
    <!-- shuffle -->
    <script src="{!! asset('assets/plugins/shuffle/shuffle.min.js') !!}" defer></script>

    {{--<!-- Google Map API Key-->--}}
    {{--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcABaamniA6OL5YvYSpB3pFMNrXwXnLwU" defer></script>--}}
    {{--<!-- Google Map Plugin-->--}}
    {{--<script src="plugins/google-map/map.js" defer></script>--}}

    <!-- Template custom -->
    <script src="{!! asset('assets/js/script.js') !!}"></script>
    @yield('footer_content')
</html>