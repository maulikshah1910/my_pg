@extends('front-end-template')

@section('page_content')
    <section class="content under-construction">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <img src="{!! asset('assets/images/under-construction.jpg') !!}" />
                </div>
            </div>
        </div>
    </section>
@endsection