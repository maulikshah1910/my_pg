@extends('front-end-template')

@section('page_content')
    <div class="banner-carousel banner-carousel-2 mb-0">
        <div class="banner-carousel-item" style="background-image:url({!! asset('assets/images/banner.webp') !!})">
            <div class="container">
                <div class="box-slider-content">
                    <div class="box-slider-text">
                        <h2 class="box-slide-title">17 Years Of Excellence In</h2>
                        <h3 class="box-slide-sub-title">Construction Industry</h3>
                        <p class="box-slide-description">You have ideas, goals, and dreams. We have a culturally diverse, forward
                            thinking team looking for talent like.</p>
                        <p>
                            <a href="#modalEnquiry" data-toggle="modal" data-target="#modalEnquiry" class="slider btn btn-primary btn-enquiry">Enquire Now</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="call-to-action no-padding">
        <div class="container">
            <div class="action-style-box">
                <div class="row">
                    <div class="col-md-8 text-center text-md-left">
                        <div class="call-to-action-text">
                            <h3 class="action-title">We understand your needs on construction</h3>
                        </div>
                    </div><!-- Col end -->
                    <div class="col-md-4 text-center text-md-right mt-3 mt-md-0">
                        <div class="call-to-action-btn">
                            <a class="btn btn-primary btn-enquiry" href="#modalEnquiry" data-toggle="modal" data-target="#modalEnquiry" id="btnEnquire">Enquire Now</a>
                        </div>
                    </div><!-- col end -->
                </div>
            </div>
        </div>
    </section>

    <section id="facts" class="facts-area dark-bg">
        <div class="container">
            <div class="facts-wrapper">
                <div class="row">
                    <div class="col-md-3 col-sm-12 ts-facts"></div>
                    <div class="col-md-3 col-sm-12 ts-facts">
                        <div class="ts-facts-img">
                            <i class="fa fa-5x fa-building"></i>
                        </div>
                        <div class="ts-facts-content">
                            <h2 class="ts-facts-num"><span class="counterUp" data-count="30">0</span></h2>
                            <h3 class="ts-facts-title">PGs Registered</h3>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12 ts-facts">
                        <div class="ts-facts-img">
                            <i class="fa fa-5x fa-users"></i>
                        </div>
                        <div class="ts-facts-content">
                            <h2 class="ts-facts-num"><span class="counterUp" data-count="124">0</span></h2>
                            <h3 class="ts-facts-title">Users Registered</h3>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12 ts-facts"></div>
                </div>
            </div>
        </div>
    </section>

    <section id="ts-service-area" class="ts-service-area pb-0">
        <div class="container">
            <div class="row text-center">
                <div class="col-12">
                    {{--<h2 class="section-title">We Are Specialists In</h2>--}}
                    <h3 class="section-sub-title">What We Do</h3>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-4">
                    <div class="ts-service-box d-flex">
                        <div class="ts-service-box-img">
                            <img loading="lazy" src="{!! asset('assets/images/icon-image/service-icon1.png') !!}" alt="service-icon">
                        </div>
                        <div class="ts-service-box-info">
                            <h3 class="service-box-title"><a href="#">Home Construction</a></h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipiscing elit Integer adipiscing erat</p>
                        </div>
                    </div><!-- Service 1 end -->

                    <div class="ts-service-box d-flex">
                        <div class="ts-service-box-img">
                            <img loading="lazy" src="{!! asset('assets/images/icon-image/service-icon2.png') !!}" alt="service-icon">
                        </div>
                        <div class="ts-service-box-info">
                            <h3 class="service-box-title"><a href="#">Building Remodels</a></h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipiscing elit Integer adipiscing erat</p>
                        </div>
                    </div><!-- Service 2 end -->

                    <div class="ts-service-box d-flex">
                        <div class="ts-service-box-img">
                            <img loading="lazy" src="{!! asset('assets/images/icon-image/service-icon3.png') !!}"  alt="service-icon">
                        </div>
                        <div class="ts-service-box-info">
                            <h3 class="service-box-title"><a href="#">Interior Design</a></h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipiscing elit Integer adipiscing erat</p>
                        </div>
                    </div><!-- Service 3 end -->

                </div><!-- Col end -->

                <div class="col-lg-4 text-center">
                    <img loading="lazy" class="img-fluid" src="{!! asset('assets/images/services/service-center.jpg') !!}" alt="service-avater-image">
                </div><!-- Col end -->

                <div class="col-lg-4 mt-5 mt-lg-0 mb-4 mb-lg-0">
                    <div class="ts-service-box d-flex">
                        <div class="ts-service-box-img">
                            <img loading="lazy" src="{!! asset('assets/images/icon-image/service-icon4.png') !!}" alt="service-icon">
                        </div>
                        <div class="ts-service-box-info">
                            <h3 class="service-box-title"><a href="#">Exterior Design</a></h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipiscing elit Integer adipiscing erat</p>
                        </div>
                    </div><!-- Service 4 end -->

                    <div class="ts-service-box d-flex">
                        <div class="ts-service-box-img">
                            <img loading="lazy" src="{!! asset('assets/images/icon-image/service-icon5.png') !!}" alt="service-icon">
                        </div>
                        <div class="ts-service-box-info">
                            <h3 class="service-box-title"><a href="#">Renovation</a></h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipiscing elit Integer adipiscing erat</p>
                        </div>
                    </div><!-- Service 5 end -->

                    <div class="ts-service-box d-flex">
                        <div class="ts-service-box-img">
                            <img loading="lazy" src="{!! asset('assets/images/icon-image/service-icon6.png') !!}" alt="service-icon">
                        </div>
                        <div class="ts-service-box-info">
                            <h3 class="service-box-title"><a href="#">Safety Management</a></h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipiscing elit Integer adipiscing erat</p>
                        </div>
                    </div><!-- Service 6 end -->
                </div><!-- Col end -->
            </div>
        </div>
    </section>

    <section class="content solid-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="column-title">Testimonials</h3>

                    <div id="testimonial-slide" class="testimonial-slide">
                        <div class="item">
                            <div class="quote-item">
                                <span class="quote-text">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor inci done idunt ut labore et dolore magna aliqua.
                                </span>

                                <div class="quote-item-footer">
                                    <img loading="lazy" class="testimonial-thumb" src="{!! asset('assets/images/clients/testimonial1.png') !!}" alt="testimonial">
                                    <div class="quote-item-info">
                                        <h3 class="quote-author">John Doe</h3>
                                        <span class="quote-subtext">Chairman, My APP</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="quote-item">
                                <span class="quote-text">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor inci done idunt ut labore et dolore magna aliqua.
                                </span>

                                <div class="quote-item-footer">
                                    <img loading="lazy" class="testimonial-thumb" src="{!! asset('assets/images/clients/testimonial1.png') !!}" alt="testimonial">
                                    <div class="quote-item-info">
                                        <h3 class="quote-author">John Doe</h3>
                                        <span class="quote-subtext">Chairman, My APP</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="quote-item">
                                <span class="quote-text">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor inci done idunt ut labore et dolore magna aliqua.
                                </span>

                                <div class="quote-item-footer">
                                    <img loading="lazy" class="testimonial-thumb" src="{!! asset('assets/images/clients/testimonial1.png') !!}" alt="testimonial">
                                    <div class="quote-item-info">
                                        <h3 class="quote-author">John Doe</h3>
                                        <span class="quote-subtext">Chairman, My APP</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" tabindex="-1" id="modalEnquiry" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Enquire Now</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="#" method="post" class="form-horizontal" id="frmEnquiry">
                    @csrf
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div id="formAlert"></div>
                            </div>
                        </div>

                        <div class="row form-group">
                            <label for="name" class="col-md-3 col-form-label">Name</label>
                            <div class="col-md-9">
                                <input type="text" name="name" id="name" class="form-control" required />
                            </div>
                        </div>
                        <div class="row form-group">
                            <label for="email" class="col-md-3 col-form-label">Email</label>
                            <div class="col-md-9">
                                <input type="text" name="email" id="email" class="form-control" required />
                            </div>
                        </div>
                        <div class="row form-group">
                            <label for="phone" class="col-md-3 col-form-label">Phone</label>
                            <div class="col-md-9">
                                <input type="text" name="phone" id="phone" class="form-control" required />
                            </div>
                        </div>
                        <div class="row form-group">
                            <label for="phone" class="col-md-3 col-form-label">Message</label>
                            <div class="col-md-9">
                                <textarea name="message" id="message" rows="4" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Send</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('footer_content')
    <script src="{!! asset('assets/plugins/validation/jquery.validate.min.js') !!}"></script>
    <script>
        $(document).on('show.bs.modal', '#modalEnquiry', function() {
            $('#frmEnquiry')[0].reset();
            $('#frmEnquiry').find('label.error').remove();
            $('#frmEnquiry').find('.form-control').removeClass('error');
            $('#formAlert').html('');
        });
        $('#frmEnquiry').validate({
            rules: {
                name: {
                    required: true,
                },
                email: {
                    required: true,
                    email: true,
                },
                phone: {
                    required: true,
                    minlength:10,
                    maxlength:10,
                },
                message: {
                    required: true,
                    minlength: 30,
                    maxlength: 500
                }
            },
        });
        $('#frmEnquiry').submit(function(e){
            e.preventDefault();
            if ($(this).valid()) {
                $form = $(this).serialize();
                console.log($form);
                $formData = new FormData($(this)[0]);
                $.ajax({
                    type: 'POST',
                    url: '{!! route('submit-enquiry') !!}',
                    data: $form,
                    success: function(response) {
                        var alert = "";
                        if (response.success == true) {
                            alert += '<div class="alert alert-success alert-dismissable fade show" role="alert">' +
                                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                                '<span aria-hidden="true">&times;</span>\n' +
                                '</button>' +
                                '<h5>Success...!</h5>Your enquiry is submitted successfully. <br />We will get back to you soon on this.' +
                                '</div>';

                            $('#frmEnquiry')[0].reset();
                            $('#frmEnquiry').find('label.error').remove();
                            $('#frmEnquiry').find('.form-control').removeClass('error');
                        } else {
                            alert += '<div class="alert alert-warning alert-dismissable fade show" role="alert">' +
                                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                                '<span aria-hidden="true">&times;</span>' +
                                '</button>' +
                                '<h5>Error...!</h5>Something went wrong while submitting your enquiry.' +
                                '<ul>' ;
                            $.each(response.errors, function(k, v) {
                                alert += '<li>'+v[0]+'</li>';
                            });
                            alert += '</ul>' +
                                '</div>';
                        }
                        $('#formAlert').html(alert);
                    }
                });
            }
        });
    </script>
@endsection