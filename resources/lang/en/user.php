<?php
return [
    'errors' => [
        'first_name' => [
            'required' => 'Please enter first name',
        ],
        'last_name' => [
            'required' => 'Please enter last name',
        ],
        'email' => [
            'required' => 'Please enter email',
            'unique' => 'This email is already registered.',
            'email' => 'Please enter valid email ID',
        ],
        'phone' => [
            'required' => 'Please enter phone number',
            'unique' => 'This phone number is already registered.',
            'digits' => 'Please enter valid phone number',
        ],
        'otp' => [
            'required' => 'Please enter OTP',
            'digits' => 'Please enter valid OTP',
            'expired' => 'OTP you have entered is expired.',
        ],
        'role' => [
            'required' => 'Please enter role',
        ],
        'gender' => [
            'required' => 'Please select gender',
        ],
        'house_no' => [
            'required' => 'Please select House No.',
        ],
        'building' => [
            'required' => 'Please select Building',
        ],
        'area' => [
            'required' => 'Please select area',
        ],
        'city' => [
            'required' => 'Please select city',
        ],
        'postcode' => [
            'required' => 'Please select postcode',
        ],
        'avatar' => [
            'required' => 'Please select Profile Picture',
        ],
        'phone_not_registered' => 'No user is registered with this phone number',
    ],
    'login' => [
        'not_found' => 'No user found for this credentials.',
        'invalid' => 'Invalid login credentials.',
        'inactive' => 'This account is inactive. Please activate an account first.',
    ],
];