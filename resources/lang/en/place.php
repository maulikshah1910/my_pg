<?php
return [
    'errors' => [
        'pg_name' => [
            'required' => 'Please Enter PG Name.',
        ],
        'house_no' => [
            'required' => 'Please select House No.',
        ],
        'building' => [
            'required' => 'Please select Building',
        ],
        'area' => [
            'required' => 'Please select area',
        ],
        'city' => [
            'required' => 'Please select city',
        ],
        'postcode' => [
            'required' => 'Please select postcode',
        ],
    ],
];