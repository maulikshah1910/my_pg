<?php
return [
    'roles' => [
        [
            'slug'=>'admin',
            'display_name' => 'Admin',
            'display_front' => false,
            'is_active' => false,
            'desc' => '',
        ],
        [
            'slug'=>'user',
            'display_name' => 'User',
            'display_front' => true,
            'is_active' => true,
            'desc' => 'I am looking for a PG/Hostel',
        ],
        [
            'slug'=>'lister',
            'display_name' => 'Listing User',
            'display_front' => true,
            'is_active' => true,
            'desc' => 'I want to list my PG/Hostel',
        ],
    ]
];