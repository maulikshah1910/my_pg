<?php
return [
    'storage' => env('FILE_STORAGE', 'local'),
    'role_status' => [
        'No',
        'Yes',
    ],
    'user_status' => [
        'Inactive',
        'Active'
    ],
    'gender' => [
        'm' => 'Male',
        'f' => 'Female',
        'o' => 'Other',
    ],
    'is_active' => [
        'No',
        'Yes',
    ],
    'cover_image' => [
        'No',
        'Yes',
    ],
    'place_status' => [
        'Draft',
        'Publish',
    ],
    'pg_for' => [
        'm' => 'Male',
        'f' => 'Female',
        'u' => 'Unisex',
    ]
];
