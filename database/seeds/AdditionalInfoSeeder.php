<?php

use Illuminate\Database\Seeder;

class AdditionalInfoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $additionalInfos = config('masters.additional_info');
        foreach ($additionalInfos as $info) {
            $additionalInfo = \App\Models\AdditionalInfo::updateOrCreate(
                [
                    'slug' => $info['slug'],
                ],
                [
                    'title' => $info['name'],
                    'is_active' => $info['active'],
                    'order' => $info['order'],
                    'control_type' => $info['control_type'],
                    'input_type' => isset($info['input_type']) ? $info['input_type'] : null,
                ]
            );
        }
    }
}
