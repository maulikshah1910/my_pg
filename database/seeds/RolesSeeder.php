<?php

use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = config('roles.roles');
        foreach($roles as $userRole) {
            $role = \App\Models\Role::updateOrCreate(
                [
                    'slug' => $userRole['slug'],
                ],
                [
                    'display_name' => $userRole['display_name'],
                    'display_front' => $userRole['display_front'],
                    'is_active' => $userRole['is_active'],
                    'desc' => $userRole['desc'],
                ]);
        }
    }
}
