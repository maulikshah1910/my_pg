<?php

use Illuminate\Database\Seeder;

class FacilitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $facilities = config('masters.facilities');
        foreach ($facilities as $facility) {
//            dd($facility);
            $facilityData = \App\Models\Facility::updateOrCreate(
                [
                    'slug' => $facility['slug'],
                ],
                [
                    'title' => $facility['name'],
                    'is_active' => $facility['active'],
                ]
            );
        }
    }
}
