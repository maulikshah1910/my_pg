<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    use SoftDeletes;

    const STATUS_ACTIVE = true;
    const STATUS_INACTIVE = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name',
        'email', 'email_verified_at',
        'phone', 'phone_verified_at',
        'password', 'otp_valid_till', 'is_active',
        'role_id',
        'gender', 'avatar',
        'building', 'landmark', 'area',
        'city', 'postcode',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'phone_verified_at' => 'datetime',
        'otp_valid_till' => 'datetime',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public static function validate($data, $id = null)
    {
        $rules = [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users,email',
            'phone' => 'required|digits:10|unique:users,phone',
            'role' => 'required',
        ];
        $messages = [
            'first_name.required' => trans('user.errors.first_name.required'),
            'last_name.required' => trans('user.errors.last_name.required'),
            'email.required' => trans('user.errors.email.required'),
            'phone.required' => trans('user.errors.phone.required'),

            'phone.digits' => trans('user.errors.phone.digits'),

            'email.unique' => trans('user.errors.email.unique'),
            'phone.unique' => trans('user.errors.phone.unique'),

            'role.required' => trans('user.errors.role.required'),

            'gender.required' => trans('user.errors.gender.required'),
            'house_no.required' => trans('user.errors.house_no.required'),
            'building.required' => trans('user.errors.building.required'),
            'area.required' => trans('user.errors.area.required'),
            'city.required' => trans('user.errors.city.required'),
            'postcode.required' => trans('user.errors.postcode.required'),
        ];

        if (isset($id)) {
//            unset($rules['email']);
//            unset($rules['phone']);
            $rules['email'] = 'email|unique:users,email,'.$id;
            $rules['phone'] = 'digits:10|unique:users,phone,'.$id;
            $rules['gender'] = 'required';
            $rules['house_no'] = 'required';
            $rules['building'] = 'required';
            $rules['area'] = 'required';
            $rules['city'] = 'required';
            $rules['postcode'] = 'required';
            unset($rules['role']);
        }

        return Validator::make($data, $rules, $messages);
    }

    public static function validateLogin($data)
    {
        $rules = [
            'phone' => 'required|digits:10',
            'otp' => 'required|digits:6',
        ];
        $messages = [
            'phone.required' => trans('user.errors.phone.required'),
            'phone.digits' => trans('user.errors.phone.digits'),
            'otp.required' => trans('user.errors.otp.required'),
            'otp.digits' => trans('user.errors.otp.digits'),
        ];

        return Validator::make($data, $rules, $messages);
    }
}
