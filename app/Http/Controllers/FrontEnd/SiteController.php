<?php

namespace App\Http\Controllers\FrontEnd;

use App\Http\Controllers\Controller;
use App\Models\Enquiry;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class SiteController extends Controller
{
    public function index()
    {
        return view('front-end.home');
    }

    public function submitEnquiry(Request $request)
    {
        try {
            $validations = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required|email',
                'phone' => 'required',
                'message' => 'required|min:30|max:500',
            ], [
                'name.required' => 'Please specify Name',
                'email.required' => 'Please specify Email',
                'email.email' => 'Please enter valid Email',
                'phone.required' => 'Please specify Phone number',
                'message.required' => 'Please specify Message',
                'message.min' => 'Message should be between 30 to 500 letters.',
                'message.max' => 'Message should be between 30 to 500 letters.',
            ]);

            if ($validations->fails()) {
                return response()->json([
                    'success' => false,
                    'errors' => $validations->getMessageBag()
                ]);
            }

            if ($request->ajax()) {
                $enquiry = Enquiry::create([
                    'name' => $request->name,
                    'email' => $request->email,
                    'phone' => $request->phone,
                    'message' => $request->message,
                ]);
                return response()->json(['success' => true]);
            } else {
                return response()->json(['success' => false]);
            }
        } catch (\Exception $ex) {
            Log::error('Error in submission of enquiry.', $ex);
            return response()->json([
                'success' => false,
                'errors' => [$ex->getMessage()]
            ]);
        }
    }

    public function terms()
    {
        return view('front-end.terms');
    }

    public function privacyPolicy()
    {
        return view('front-end.privacy-policy');
    }
}
