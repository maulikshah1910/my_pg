<?php

namespace App\Http\Controllers\API;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\AdditionalInfo;
use App\Models\Facility;
use App\Models\Place;
use App\Models\PlaceAdditionalInfo;
use App\Models\PlaceFacility;
use App\Models\PlaceImage;
use App\Transformers\AdditionalInfoTransformer;
use App\Transformers\FacilityTransformer;
use App\Transformers\PlaceTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Spatie\Fractal\Fractal;
use Tymon\JWTAuth\Facades\JWTAuth;

class ListingController extends Controller
{
    public function getMasters()
    {
        $facilities = Facility::where('is_active', Facility::STATUS_ACTIVE)
            ->orderBy('title', 'ASC')
            ->get();
        $facilityCollection = Fractal::create($facilities, FacilityTransformer::class);

        $additionalInfos = AdditionalInfo::where('is_active', AdditionalInfo::STATUS_ACTIVE)
            ->orderBy('order', 'ASC')
            ->get();
        $additionalInfoCollection = Fractal::create($additionalInfos, AdditionalInfoTransformer::class);

        return response()->json([
            'success' => true,
            'data' => [
                'facilities' => $facilityCollection->toArray()['data'],
                'additional_info' => $additionalInfoCollection->toArray()['data']
            ],
        ]);
    }

    public function storeListing(Request $request)
    {
        try {
            $token = JWTAuth::parseToken()->authenticate();
            $validation = Place::validate($request->all());
            if ($validation->fails()) {
                $messages = [];
                foreach ($validation->messages()->toArray() as $message) {
                    $messages[] = $message[0];
                }
                return response()->json([
                    'success' => false,
//                    'message' => $validation->messages()->first(),
                    'message' => implode('<br />', $messages)
                ]);
            }

            DB::beginTransaction();
            // save the place first
            $place = Place::create([
                'owner_id' => $token->id,
                'house_no' => $request->house_no,
                'pg_name' => $request->pg_name,
                'pg_for' => $request->pg_for,
                'building' => $request->building,
                'landmark' => $request->landmark,
                'area' => $request->area,
                'city' => $request->city,
                'postcode' => $request->postcode,
                'latitude' => $request->has('latitude') ? $request->latitude : null,
                'longitude' => $request->has('longitude') ? $request->longitude : null,
                'status' => Place::STATUS_DRAFT,
                'is_active' => Place::STATUS_ACTIVE,
            ]);

            // store place facilities
            if ($request->has('facilities')) {
                $facilities = json_decode($request->facilities);
                foreach ($facilities as $facility) {
                    PlaceFacility::create([
                        'place_id' => $place->id,
                        'facility_id' => $facility
                    ]);
                }
            }
            // store place additional info
            if($request->has('additional_info')) {
                $addInfo = json_decode($request->additional_info, true);
                foreach($addInfo as $info=>$value) {
                    PlaceAdditionalInfo::create([
                        'place_id' => $place->id,
                        'info_id' => $info,
                        'value' => $value,
                    ]);
                }
            }
            // store place images
            if ($request->image_count > 0) {
                $storageType = Helper::getStorage();
                for ($i = 0; $i < $request->image_count; $i++) {
                    $imageFile = $request->file('place_image_' . $i);

                    $fileName = Helper::getListingImagesPath()
                        . '/' . md5(time())
                        . '.' . $imageFile->getClientOriginalExtension();
                    Storage::disk($storageType)->put($fileName, file_get_contents($imageFile), 'public');

                    if ($storageType == 'local') {
                        $fileURL = url(Storage::disk($storageType)->url($fileName));
                    } else {
                        $fileURL = Storage::disk($storageType)->url($fileName);
                    }

                    PlaceImage::create([
                        'place_id' => $place->id,
                        'url' => $fileURL,
                    ]);
                }
            }

            DB::commit();

            return response()->json([
                'success' => true,
                'data' => (new PlaceTransformer())->transform($place)
            ]);
        } catch (\Exception $ex) {
            Helper::writeErrorLog('Error in store place listing.', $ex);
            return response()->json([
                'success' => false,
                'message' => $ex->getMessage(),
            ]);
        }
    }

    public function updateListing(Request $request, $id)
    {
        try {
            $token = JWTAuth::parseToken()->authenticate();

            $place = Place::find($id);
            if (!$place instanceof Place) {
                return response()->json([
                    'success' => false,
                    'message' => 'Place not found...'
                ]);
            }

            if ($request->has('image_count')) {
                if ($request->image_count > 5) {
                    return response()->json([
                        'success' => false,
                        'message' => 'You cannot upload more than 5 images.'
                    ]);
                }
                $count = $request->image_count;
                $currentImagesCount = $place->images->count();
                if ($currentImagesCount == 5) {
                    return response()->json([
                        'success' => false,
                        'message' => 'Your list has 5 images. You cannot upload more images.'
                    ]);
                }
                $possibleCount = $count + $currentImagesCount;
                if ($possibleCount > 5) {
                    return response()->json([
                        'success' => false,
                        'message' => "Your place already has $currentImagesCount images. You can add only " . (5 - (int)$currentImagesCount) . " more images"
                    ]);
                }
            }

            DB::beginTransaction();
            // update listing info
            if ($request->has('house_no')) {
                $place->house_no = $request->house_no;
            }
            if ($request->has('pg_name')) {
                $place->pg_name = $request->pg_name;
            }
            if ($request->has('pg_for')) {
                $place->pg_for = $request->pg_for;
            }
            if ($request->has('building')) {
                $place->building = $request->building;
            }
            if ($request->has('landmark')) {
                $place->landmark = $request->landmark;
            }
            if ($request->has('area')) {
                $place->area = $request->area;
            }
            if ($request->has('city')) {
                $place->city = $request->city;
            }
            if ($request->has('postcode')) {
                $place->postcode = $request->postcode;
            }
            $place->latitude = $request->has('latitude') ? $request->latitude : null;
            $place->longitude = $request->has('longitude') ? $request->longitude : null;
            $place->save();

            // store place facilities
            if ($request->has('facilities')) {
                PlaceFacility::where('place_id', $id)->delete();
                $facilities = json_decode($request->facilities);
                foreach ($facilities as $facility) {
                    PlaceFacility::create([
                        'place_id' => $place->id,
                        'facility_id' => $facility
                    ]);
                }
            }
            // store place additional info
            if($request->has('additional_info')) {
                PlaceAdditionalInfo::where('place_id', $id)->delete();
                $addInfo = json_decode($request->additional_info, true);
                foreach($addInfo as $item) {
                    $info = array_keys($item)[0];
                    $value = $item[$info];
                    PlaceAdditionalInfo::create([
                        'place_id' => $place->id,
                        'info_id' => $info,
                        'value' => $value,
                    ]);
                }
            }
            // store place images
            if ($request->image_count > 0) {
                $storageType = Helper::getStorage();
                for ($i = 0; $i < $request->image_count; $i++) {
                    $imageFile = $request->file('place_image_' . $i);

                    $fileName = Helper::getListingImagesPath()
                        . '/' . md5(time())
                        . '.' . $imageFile->getClientOriginalExtension();
                    Storage::disk($storageType)->put($fileName, file_get_contents($imageFile), 'public');

                    if ($storageType == 'local') {
                        $fileURL = url(Storage::disk($storageType)->url($fileName));
                    } else {
                        $fileURL = Storage::disk($storageType)->url($fileName);
                    }

                    PlaceImage::create([
                        'place_id' => $place->id,
                        'url' => $fileURL,
                    ]);
                }
            }
            DB::commit();

            return response()->json([
                'success' => true,
                'data' => (new PlaceTransformer())->transform($place)
            ]);
        } catch (\Exception $ex) {
            Helper::writeErrorLog('Error in store place listing.', $ex);
            return response()->json([
                'success' => false,
                'message' => $ex->getMessage(),
            ]);
        }
    }

    public function getListing($id)
    {
        try {
            $place = Place::find($id);
            if (!$place instanceof Place) {
                return response()->json([
                    'success' => false,
                    'message' => 'Place not found...'
                ]);
            }
            return response()->json([
                'success' => true,
                'data' => (new PlaceTransformer())->transform($place)
            ]);
        } catch (\Exception $ex) {
            Helper::writeErrorLog('Error in store place listing.', $ex);
            return response()->json([
                'success' => false,
                'message' => $ex->getMessage(),
            ]);
        }
    }

    public function publishListing($id)
    {
        try {
            $place = Place::find($id);
            if (!$place instanceof Place) {
                return response()->json([
                    'success' => false,
                    'message' => 'Place not found...'
                ]);
            }

            if ($place->status != Place::STATUS_DRAFT) {
                return response()->json([
                    'success' => false,
                    'message' => 'This is not draft. Cannot Publish this listing.'
                ]);
            }

            $place->status = Place::STATUS_PUBLISH;
            $place->save();
            return response()->json([
                'success' => true,
                'data' => (new PlaceTransformer())->transform($place)
            ]);
        } catch (\Exception $ex) {
            Helper::writeErrorLog('Error in publish listing.', $ex);
            return response()->json([
                'success' => false,
                'message' => $ex->getMessage(),
            ]);
        }
    }

    public function deletePlaceImage(Request $request)
    {
        try {
            $validations = [
                'place_id' => 'required',
                'image_id' => 'required',
            ];

            $validation = Validator::make($request->all(), $validations);
            if ($validation->fails()) {
                $messages = [];
                foreach ($validation->messages()->toArray() as $message) {
                    $messages[] = $message[0];
                }
                return response()->json([
                    'success' => false,
//                    'message' => $validation->messages()->first(),
                    'message' => implode('<br />', $messages)
                ]);
            }

            $placeID = $request->place_id;
            $imageID = $request->image_id;

            $placeImage = PlaceImage::where('place_id', $placeID)
                ->where('id', $imageID)
                ->first();
            if (!$placeImage instanceof PlaceImage) {
                return response()->json([
                    'success' => false,
                    'message' => 'Could not find image to delete...'
                ]);
            }

            // delete image from URL
            DB::beginTransaction();
            $imageURL = $placeImage->url;

            $storageType = Helper::getStorage();
            Storage::disk($storageType)->delete(Helper::getListingImagesPath() . '/' . basename($imageURL));

            $placeImage->forceDelete();
            DB::commit();

            $place = Place::find($placeID);
            return response()->json([
                'success' => true,
                'data' => (new PlaceTransformer())->transform($place)
            ]);
        } catch (\Exception $ex) {
            Helper::writeErrorLog('Error in delete place image.', $ex);
            return response()->json([
                'success' => false,
                'message' => $ex->getMessage(),
            ]);
        }
    }

    public function getUserPlaceList()
    {
        try {
            $token = JWTAuth::parseToken()->authenticate();

            // get user places
            $userPlaces = Place::where('owner_id', $token->id)->orderBy('created_at', 'ASC')->get();

            $placeCollection = Fractal::create($userPlaces, PlaceTransformer::class);
            return response()->json([
                'success' => true,
                'data' => $placeCollection->toArray()['data']
            ]);
        } catch (\Exception $ex) {
            Helper::writeErrorLog('Error in getting user place list.', $ex);
            return response()->json([
                'success' => false,
                'message' => $ex->getMessage(),
            ]);
        }
    }
}
