<?php

namespace App\Http\Controllers\API;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Transformers\RoleTransformer;
use App\Transformers\UserTransformer;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Spatie\Fractal\Fractal;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
    public function userRoles()
    {
        try {
            $roles = Role::where('display_front', Role::DISPLAY_FRONT_ON)
                ->where('is_active', Role::STATUS_ACTIVE)
                ->orderBy('display_name', 'ASC')
                ->get();
            $rolesCollection = Fractal::create($roles, RoleTransformer::class);
            return response()->json([
                'success' => true,
//                'data' => $rolesCollection,
                'data' => $rolesCollection->toArray()['data'],
            ]);
        } catch (\Exception $ex) {
            Helper::writeErrorLog('Something went wrong while getting user roles: ', $ex);
            return response()->json([
                'success' => false,
                'error' => $ex->getMessage(),
            ]);
        }
    }

    public function register(Request $request)
    {
        try {
            $validation = User::validate($request->all());

            if ($validation->fails()) {
                $messages = [];
                foreach ($validation->messages()->toArray() as $message) {
                    $messages[] = $message[0];
                }
                return response()->json([
                    'success' => false,
//                    'message' => $validation->messages()->first(),
                    'message' => implode('<br />', $messages)
                ]);
            }

            // generate OTP
            $userOtp = self::generateOTP();

            DB::beginTransaction();
            // create a user
            $user = User::create([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'email' => $request->email,
                'phone' => $request->phone,
                'phone_verified_at' => Carbon::now(),
                'password' => Hash::make($userOtp),
                'otp_valid_till' => Carbon::now()->addMinutes(10),
                'is_active' => User::STATUS_ACTIVE,
                'role_id' => $request->role,
            ]);

            // send OTP to user
            DB::commit();
            return response()->json([
                'success' => true,
                'otp' => $userOtp,
                'user' => (new UserTransformer())->transform($user),
            ]);
        } catch (\Exception $ex) {
            Helper::writeErrorLog('Error in user registration.', $ex);
            return response()->json([
                'success' => false,
                'message' => $ex->getMessage(),
            ]);
        }
    }

    public function login(Request $request)
    {
        try {
            $validate = User::validateLogin($request->all());
            if ($validate->fails()) {
                $messages = [];
                foreach ($validate->messages()->toArray() as $message) {
                    $messages[] = $message[0];
                }
                return response()->json([
                    'success' => false,
//                    'message' => $validate->messages()->first()
                    'message' => implode('<br />', $messages)
                ]);
            }

            $user = User::where('phone', $request->phone)->first();
            if (!$user instanceof User) {
                return response()->json([
                    'success' => false,
                    'message' => 'No user found matching this credentials.'
                ]);
            }
            if ($user->is_active == User::STATUS_INACTIVE) {
                return response()->json([
                    'success' => false,
                    'message' => 'This user is not active. Please activate your account.'
                ]);
            }

//            if (Carbon::make($user->otp_valid_till)->diffInMinutes(Carbon::now()) > 10) {
//                return response()->json([
//                    'success' => false,
//                    'message' => trans('user.errors.otp.expired')
//                ]);
//            }

            $otp = $request->otp;
            $request->merge([
                'password' => $otp
            ]);
            $credentials = $request->only('phone', 'password');
            $authToken = JWTAuth::attempt($credentials);
            if($authToken) {
                return response()->json([
                    'success' => true,
                    'token' => $authToken,
                    'data' => (new UserTransformer())->transform($user)
                ]);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Invalid credentials. Please try again'
                ]);
            }
        } catch (\Exception $ex) {
            Helper::writeErrorLog('Error in login:', $ex);
            return response()->json([
                'success' => false,
                'message' => $ex->getMessage()
            ]);
        }
    }

    public function logout(Request $request)
    {
        try {
            if (JWTAuth::invalidate(JWTAuth::getToken())) {
                Auth::logout();
                return response()->json([
                    'success' => true,
                ], 200);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Failed to logout user. Try again.'
                ], 500);
            }
        } catch (\Exception $ex) {
            Helper::writeErrorLog('Error in logout()', $ex);
        }
    }

    public function profile()
    {
        try {
            $token = JWTAuth::parseToken()->authenticate();
            $user = User::find($token->id);
            return response()->json([
                'success' => true,
                'data' => (new UserTransformer())->transform($user)
            ]);
        } catch (\Exception $e) {
            Helper::writeErrorLog('Something went wrong in getting user profile', $e);
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ], 200);
        }
    }

    public function updateProfile(Request $request)
    {
        try {
            $token = JWTAuth::parseToken()->authenticate();

            $validate = User::validate($request->all(), $token->id);
            if ($validate->fails()) {
                $messages = [];
                foreach ($validate->messages()->toArray() as $message) {
                    $messages[] = $message[0];
                }
                return response()->json([
                    'success' => false,
                    // 'message' => $validate->messages(),
                    'message' => implode('<br />', $messages)
                ]);
            }

            $user = User::find($token->id);
            DB::beginTransaction();

            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;

            $user->gender = strtolower($request->gender);
            $user->house_no = $request->house_no;
            $user->building = $request->building;
            $user->landmark = $request->landmark;
            $user->area = $request->area;
            $user->city = $request->city;
            $user->postcode = $request->postcode;

            $user->save();

            DB::commit();

            return response()->json([
                'success' => true,
                'data' => (new UserTransformer())->transform($user)
            ]);
        } catch (\Exception $e) {
            Helper::writeErrorLog('Error in updating user profile.', $e);
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
            ]);
        }
    }

    public function setUserAvatar(Request $request)
    {
        try {
            $token = JWTAuth::parseToken()->authenticate();

            if (!$request->hasFile('avatar')) {
                return response()->json([
                    'success' => false,
                    'message' => trans('user.errors.avatar.required')
                ]);
            }

            $user = User::find($token->id);
            DB::beginTransaction();

            $storageType = Helper::getStorage();
            $avatarFile = Helper::getAvatarPath() . '/' . md5(time()) . '.' . $request->file('avatar')->getClientOriginalExtension();

            Storage::disk($storageType)
                ->put($avatarFile, file_get_contents($request->file('avatar')), 'public');

            if ($storageType == 'local') {
                $avatarLocation = url(Storage::disk($storageType)->url($avatarFile));
            } else {
                $avatarLocation = Storage::disk($storageType)->url($avatarFile);
            }

            $currentAvatar = $user->avatar;
            if (isset($currentAvatar)) {
                Storage::disk($storageType)->delete(Helper::getAvatarPath() . '/' . basename($currentAvatar));
            }
            $user->avatar = $avatarLocation;
            $user->save();

            DB::commit();
            $user = User::find($token->id);
            return response()->json([
                'success' => true,
                'data' => (new UserTransformer())->transform($user)
            ]);
        } catch (\Exception $e) {
            Helper::writeErrorLog('Error in updating user profile.', $e);
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
            ]);
        }
    }

    public function sendOtp(Request $request)
    {
        try {
            if (!$request->has('phone')) {
                return response()->json([
                    'success' => false,
                    'message' => trans('user.errors.phone.required')
                ]);
            }
            $user = User::where('phone', $request->phone)->first();
            if (!$user instanceof User) {
                return response()->json([
                    'success' => false,
                    'message' => trans('user.errors.phone_not_registered')
                ]);
            }

            $userOtp = self::generateOTP();
            DB::beginTransaction();
            $user->password = Hash::make($userOtp);
            $user->otp_valid_till = Carbon::now()->addMinutes(10);
            $user->save();
            DB::commit();
            return response()->json([
                'success' => true,
                'otp' => $userOtp,
            ]);
        } catch (\Exception $ex) {
            Helper::writeErrorLog('Error in sending OTP', $ex);
            return respose()->json([
                'success' => false,
                'message' => $ex->getMessage(),
            ]);
        }
    }

    private function generateOTP()
    {
        return mt_rand(111111,999999);
    }
}
