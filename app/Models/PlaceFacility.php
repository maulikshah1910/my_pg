<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlaceFacility extends Model
{
    protected $fillable = [
        'place_id', 'facility_id'
    ];
    public $timestamps = false;

    public function place()
    {
        return $this->belongsTo(Place::class, 'place_id', 'id');
    }

    public function facility()
    {
        return $this->belongsTo(Facility::class, 'facility_id', 'id');
    }
}
