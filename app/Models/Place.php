<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Validator;

class Place extends Model
{
    use SoftDeletes;
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    const STATUS_DRAFT = 0;
    const STATUS_PUBLISH = 1;
    const STATUS_EXPIRED = 2;

    protected $fillable = [
        'owner_id',
        'pg_name',
        'pg_for',
        'house_no', 'building', 'landmark', 'area',
        'city', 'postcode',
        'latitude', 'longitude',
        'status', 'is_active',
    ];

    public function images()
    {
        return $this->hasMany(PlaceImage::class, 'place_id', 'id');
    }

    public function facilities()
    {
        return $this->hasMany(PlaceFacility::class, 'place_id', 'id');
    }

    public function additionalInfo()
    {
        return $this->hasMany(PlaceAdditionalInfo::class, 'place_id', 'id');
    }

    public static function validate($data, $id = null)
    {
        $rules = [
            'pg_name' => 'required',
            'house_no' => 'required',
            'building' => 'required',
            'area' => 'required',
            'city' => 'required',
            'postcode' => 'required',
        ];
        $messages = [
            'pg_name.required' => trans('place.errors.pg_name.required'),
            'house_no.required' => trans('place.errors.house_no.required'),
            'building.required' => trans('place.errors.building.required'),
            'area.required' => trans('place.errors.area.required'),
            'city.required' => trans('place.errors.city.required'),
            'postcode.required' => trans('place.errors.postcode.required'),
        ];
        return Validator::make($data, $rules, $messages);
    }
}
