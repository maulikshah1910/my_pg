<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PlaceImage extends Model
{
    use SoftDeletes;

    const STATUS_IS_ACTIVE = 1;
    const STATUS_IS_INACTIVE = 0;
    const IS_COVER = 1;
    const IS_NOT_COVER = 0;

    protected $fillable = [
        'place_id', 'url', 'is_cover', 'is_active'
    ];

    public function place()
    {
        return $this->belongsTo(Place::class, 'place_id', 'id');
    }
}
