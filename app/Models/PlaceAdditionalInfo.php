<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PlaceAdditionalInfo extends Model
{
    protected $fillable = [
        'place_id', 'info_id', 'value'
    ];
    public $timestamps = false;

    public function place()
    {
        return $this->belongsTo(Place::class, 'place_id', 'id');
    }

    public function info()
    {
        return $this->belongsTo(AdditionalInfo::class, 'info_id', 'id');
    }
}
