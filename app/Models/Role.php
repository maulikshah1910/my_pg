<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use SoftDeletes;

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    const DISPLAY_FRONT_ON = 1;
    const DISPLAY_FRONT_OFF = 0;

    protected $fillable = [
        'slug', 'display_name', 'display_front', 'is_active'
    ];

    public function users()
    {
        return $this->hasMany(User::class, 'role_id', 'id');
    }
}
