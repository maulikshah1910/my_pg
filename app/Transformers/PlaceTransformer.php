<?php
namespace App\Transformers;

use App\Models\Place;
use App\Models\PlaceAdditionalInfo;
use App\Models\PlaceFacility;
use App\Models\PlaceImage;
use Illuminate\Support\Facades\File;
use League\Fractal\TransformerAbstract;

class PlaceTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [];

    public function transform(Place $place)
    {
        $data = [
            'id' => $place->id,
            'pg_name' => $place->pg_name,
            'pg_for' => $place->pg_for,
            'house_no' => $place->house_no,
            'building' => $place->building,
            'landmark' => isset($place->landmark) ? $place->landmark : '',
            'area' => $place->area,
            'city' => $place->city,
            'postcode' => $place->postcode,
            'latitude' => isset($place->latitude) ? $place->latitude : '0.00' ,
            'longitude' => isset($place->longitude) ? $place->longitude : '0.00',

            'status' => config('settings.place_status')[$place->status],
            'active' => config('settings.is_active')[$place->is_active]
        ];

        // get place images
        $images = [];
        $placeImages = PlaceImage::where('place_id', $place->id)->get();
        foreach($placeImages as $image) {
            $images[] = [
                'id' => $image->id,
                'url' => $image->url,
                'cover_image' => config('settings.cover_image')[$image->is_cover]
            ];
        }
        $data['place_images'] = $images;

        // get place facilities
        $facilities = [];
        $placeFacilities = PlaceFacility::with('facility')
            ->where('place_id', $place->id)->get();
        foreach ($placeFacilities as $facility) {
            $facilityIcon =  (File::exists(public_path('assets/images/icons/facilities/'.$facility->facility->slug.'.png'))) ? asset('assets/images/icons/facilities/') . '/' . $facility->facility->slug . '.png' :  asset('assets/images/icons/facilities/wifi.png');
            $facilities[] = [
                'id' => $facility->facility_id,
                'title' => $facility->facility->title,
                'icon' => $facilityIcon,
            ];
        }
        $data['place_facilities'] = $facilities;

        // get place additional information
        $additionalInfo = [];
        $placeInfo = PlaceAdditionalInfo::with('info')
            ->where('place_id', $place->id)->get();
        foreach($placeInfo as $info) {
            $additionalInfo[] = [
                'info_id' => $info->info_id,
                'info_title' => $info->info->title,
                'value' => $info->value,
            ];
        }
        $data['place_additional_info'] = $additionalInfo;

        // get min rent amount for place
        $minRent = null;
        // $rentData = PlaceAdditionalInfo::with('info')
        //     ->whereHas('info', function($query) {
        //         $query->where('slug', 'LIKE', '%rent');
        //         $query->where('input_type', 'number');
        //     })
        //     ->where('place_id', $place->id)
        //     ->get();
        $rentData = PlaceAdditionalInfo::join('additional_infos', 'additional_infos.id', '=', 'place_additional_infos.info_id')
            ->where('additional_infos.slug', 'LIKE', '%rent')
            ->where('additional_infos.input_type', 'number')
            ->where('place_additional_infos.place_id', $place->id)
            ->select('place_additional_infos.*')
            ->get();
            
        foreach($rentData as $rentInfo) {
            if (is_null($minRent)) {
                $minRent = $rentInfo->value;
            }
            if ($minRent > (float)$rentInfo->value) {
                $minRent = (float)$rentInfo->value;
            }
        }
        if ($minRent > 0) {
            $data['min_rent'] = $minRent;
        }


        return $data;
    }
}