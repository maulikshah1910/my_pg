<?php
namespace App\Transformers;

use App\User;
use Illuminate\Support\Facades\File;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [];

    public function transform(User $user)
    {
        $userAvatarDir = public_path('uploads/users/');
        $data = [
            'id' => $user->id,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'email' => $user->email,
            'email_verified' => (isset($user->email_verified_at)) ? 'Yes' : 'No',
            'phone' => $user->phone,
            'phone_verified' => (isset($user->phone_verified_at)) ? 'Yes' : 'No',
            'gender' => isset($user->gender) ? config('settings.gender')[$user->gender] : 'Male',
            // 'avatar' => isset($user->avatar) ? asset('uploads/users/' . $user->avatar) : asset('assets/images/icons/account-lister.png'),
            'house_no' => isset($user->house_no) ? $user->house_no : '',
            'building' => $user->building,
            'landmark' => isset($user->landmark) ? $user->landmark : '',
            'area' => $user->area,
            'city' => $user->city,
            'postcode' => $user->postcode,

            'status' => config('settings.user_status')[$user->is_active],
        ];

        $userAvatar = asset('assets/images/icons/account-lister.png');
        if (isset($user->avatar)) {
            $userAvatar = $user->avatar;
        }
        $data['avatar'] = $userAvatar;

        return $data;
    }
}