<?php
namespace App\Transformers;

use App\Models\Role;
use Illuminate\Support\Facades\File;
use League\Fractal\TransformerAbstract;

class RoleTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [];

    public function transform(Role $role)
    {
        $data = [
            'id' => $role->id,
            'name' => $role->display_name,
            'active' => config('settings.role_status')[$role->is_active],
            'info' => $role->desc,
            'icon' => (File::exists(public_path('assets/images/icons/account-'.$role->slug.'.png'))) ? asset('assets/images/icons/account-') . $role->slug . '.png' :  asset('assets/images/icons/account-lister.png'),
        ];

        return $data;
    }
}