<?php
namespace App\Transformers;

use App\Models\AdditionalInfo;
use League\Fractal\TransformerAbstract;

class AdditionalInfoTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [];

    public function transform(AdditionalInfo $additionalInfo)
    {
        $data = [
            'id' => $additionalInfo->id,
            'title' => $additionalInfo->title,
            'control_type' => $additionalInfo->control_type,
            'active' => config('settings.is_active')[$additionalInfo->is_active],
        ];
        if (isset($additionalInfo->input_type)) {
            $data['input_type'] = $additionalInfo->input_type;
        }

        return $data;
    }
}