<?php
namespace App\Transformers;


use App\Models\Facility;
use Illuminate\Support\Facades\File;
use League\Fractal\TransformerAbstract;

class FacilityTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [];

    public function transform(Facility $facility)
    {
        $facilityIcon =  (File::exists(public_path('assets/images/icons/facilities/' . '/' .$facility->slug.'.png'))) ? asset('assets/images/icons/facilities/') . '/' . $facility->slug . '.png' :  asset('assets/images/icons/facilities/wifi.png');
        $data = [
            'id' => $facility->id,
            'title' => $facility->title,
            'active' => config('settings.is_active')[$facility->is_active],
            'icon' => $facilityIcon,
        ];

        return $data;
    }
}