<?php
/**
 * Created by PhpStorm.
 * User: er
 * Date: 16/2/22
 * Time: 10:06 PM
 */

namespace App\Helpers;


use Illuminate\Support\Facades\Log;

class Helper
{
    public static function writeInfoLog($message)
    {
        Log::info($message);
    }

    public static function writeErrorLog($message, \Exception $e)
    {
        Log::error($message, $e->getTrace());
    }

    public static function getStorage()
    {
        return config('settings.storage');
    }

    public static function getAvatarPath()
    {
        $storageType = self::getStorage();
        if ($storageType == 'local') {
            return  public_path() . '/uploads/users';
            return 'public/uploads/users';
        } else {
            return 'users';
        }
    }

    public static function getListingImagesPath()
    {
        $storageType = self::getStorage();
        if ($storageType == 'local') {
            return 'public/uploads/listings';
        } else {
            return 'listings';
        }
    }
}