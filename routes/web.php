<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FrontEnd\SiteController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontEnd\SiteController@index');
Route::post('submit-enquiry', 'FrontEnd\SiteController@submitEnquiry')->name('submit-enquiry');

Route::get('terms', [SiteController::class, 'terms'])->name('terms');
Route::get('privacy-policy', [SiteController::class, 'privacyPolicy'])->name('privacy-policy');

Route::get('system-logs', [\Rap2hpoutre\LaravelLogViewer\LogViewerController::class, 'index']);

Route::get('phpinfo', function(){
    phpinfo();
});