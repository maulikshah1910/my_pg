<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\ListingController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('roles', [AuthController::class, 'userRoles'])->name('api.roles');
Route::post('register', [AuthController::class, 'register'])->name('api.register');
Route::post('login', [AuthController::class, 'login'])->name('api.login');
Route::post('otp/send', [AuthController::class, 'sendOtp'])->name('api.otp.send');

Route::group(['middleware' => ['jwt.verify']], function() {
    Route::get('logout', [AuthController::class, 'logout'])->name('api.logout');
    Route::get('profile', [AuthController::class, 'profile'])->name('api.profile');
    Route::post('profile/update', [AuthController::class, 'updateProfile'])->name('api.profile.update');
    Route::post('profile/avatar/update', [AuthController::class, 'setUserAvatar'])->name('api.profile.avatar.update');

    /*
     * get Masters
     */
    Route::get('master/listing', [ListingController::class, 'getMasters'])->name('master.listing');

    /*
     * Listing APIs
     */
    Route::post('listing/create', [ListingController::class, 'storeListing'])->name('listing.create');
    Route::get('listing/{id}', [ListingController::class, 'getListing'])->name('listing.info');
    Route::post('listing/{id}/update', [ListingController::class, 'updateListing'])->name('listing.update');
    Route::post('listing/{id}/publish', [ListingController::class, 'publishListing'])->name('listing.publish');

    Route::delete('listing/image/delete', [ListingController::class, 'deletePlaceImage'])->name('listing.image.delete');

    Route::get('my-listing', [ListingController::class, 'getUserPlaceList'])->name('listing.user-listing');
});